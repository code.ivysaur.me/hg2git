# hg2git

A wrapper script to convert a mercurial repository to git using `mercurial-git`.

## Usage

```
hg2git ./my-mercurial-repo/
```

## Features

- Handle necessary bare-repo intermediate state
- Handle necessary master hg bookmark
- Preserve ignored files in working directory
- Clean up hgtags files
- Partial conversion of hgignore to gitignore

## Changelog

2018-10-06 v1.0.0
- Initial public release
